/*
  Sensorless control using SVPWM
  Auture: Aufa Ch.
  last edit: 18 / 09 / 2017
*/
const int EN1 = 2;
const int EN2 = 3;
const int EN3 = 4;
 
const int IN1 = 6;
const int IN2 = 7;
const int IN3 = 8; 
 
const int pwmSin[] = {128, 132, 136, 140, 143, 147, 151, 155, 159, 162, 166, 170, 174, 178, 181, 185, 
189, 192, 196, 200, 203, 207, 211, 214, 218, 221, 225, 228, 232, 235, 238, 239, 240, 241, 242, 243, 
244, 245, 246, 247, 248, 248, 249, 250, 250, 251, 252, 252, 253, 253, 253, 254, 254, 254, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254, 254, 254, 253, 253, 253, 252, 252, 251, 
250, 250, 249, 248, 248, 247, 246, 245, 244, 243, 242, 241, 240, 239, 238, 239, 240, 241, 242, 243, 
244, 245, 246, 247, 248, 248, 249, 250, 250, 251, 252, 252, 253, 253, 253, 254, 254, 254, 255, 255, 
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254, 254, 254, 253, 253, 253, 252, 252, 251, 
250, 250, 249, 248, 248, 247, 246, 245, 244, 243, 242, 241, 240, 239, 238, 235, 232, 228, 225, 221, 
218, 214, 211, 207, 203, 200, 196, 192, 189, 185, 181, 178, 174, 170, 166, 162, 159, 155, 151, 147, 
143, 140, 136, 132, 128, 124, 120, 116, 113, 109, 105, 101, 97, 94, 90, 86, 82, 78, 75, 71, 67, 64, 
60, 56, 53, 49, 45, 42, 38, 35, 31, 28, 24, 21, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 8, 7, 6, 6, 
5, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 6, 6, 7, 
8, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 8, 7, 6, 6, 5, 4, 
4, 3, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 6, 6, 7, 8, 8, 
9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21, 24, 28, 31, 35, 38, 42, 45, 49, 53, 56, 60, 64, 67, 71, 75, 
78, 82, 86, 90, 94, 97, 101, 105, 109, 113, 116, 120, 124};
 
int currentStepA;
int currentStepB;
int currentStepC;
int sineArraySize;
int increment = 0;
boolean direct = 1; // direction true=forward, false=backward

int wait = 10000;
int s_wait = A7;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
unsigned long Time,count;

int stepCount = 0;
int stepMax = 6;
 
void setup() {
  Serial.begin(115200);
  // Increase PWM frequency to 32 kHz  (make unaudible)
  TCCR4B = TCCR4B & 0b11111000 | 0x01; 
  
  pinMode(IN1, OUTPUT); 
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  
  pinMode(EN1, OUTPUT); 
  pinMode(EN2, OUTPUT); 
  pinMode(EN3, OUTPUT); 

  digitalWrite(EN1, HIGH);
  digitalWrite(EN2, HIGH);
  digitalWrite(EN3, HIGH);

  pinMode(s_wait,INPUT);

  sineArraySize = sizeof(pwmSin)/sizeof(int); // Find lookup table size //get number array
  int phaseShift = sineArraySize / 3;         // Find phase shift and initial A, B C phase values
  currentStepA = 0;
  currentStepB = currentStepA + phaseShift;
  currentStepC = currentStepB + phaseShift;
  sineArraySize--; // Convert from array Size to last PWM array number
  
  //Serial.println(sineArraySize);
  //Start holding
  analogWrite(IN1, pwmSin[currentStepA]);
  analogWrite(IN2, pwmSin[currentStepB]);
  analogWrite(IN3, pwmSin[currentStepC]); 
  delay(3000);
}

void loop() {
  //input potentiometer for map value delay with micro()
  wait = map(analogRead(s_wait),0,1023,10000,1000);
  Serial.print(166666.667/wait);
  Serial.println(" RPM");
  
  analogWrite(IN1, pwmSin[currentStepA]);
  analogWrite(IN2, pwmSin[currentStepB]);
  analogWrite(IN3, pwmSin[currentStepC]);  
  
  if (direct==true) increment = 1; //direction input !!!!!
  else increment = -1;     

  Time = micros();
  if(Time - count >= wait){
      count = Time;
      currentStepA = currentStepA + increment;
      currentStepB = currentStepB + increment;
      currentStepC = currentStepC + increment;
  }
  //Check for lookup table overflow and return to opposite end if necessary
  if(currentStepA > sineArraySize){
      if(stepCount >= stepMax){
      currentStepA = sineArraySize; 
      direct = 0;
      stepCount = 0;
      delay(2000); //waiting for look hardware
      }
      else{
         currentStepA = 0;
         direct = 1;
         stepCount += 1;
      }
  }
  if(currentStepA < 0){
      if(stepCount >= stepMax){
      currentStepA = 0; 
      direct = 1;
      stepCount = 0;
      }
      else{
      currentStepA = sineArraySize; 
      direct = 0;
      stepCount += 1; 
      }
  } 
 
  if(currentStepB > sineArraySize)  currentStepB = 0;
  if(currentStepB < 0)  currentStepB = sineArraySize;
 
  if(currentStepC > sineArraySize)  currentStepC = 0;
  if(currentStepC < 0) currentStepC = sineArraySize;
   
}

