/*
  Sensorless control using direct back EMF
  Auture: Aufa Ch.
  last edit: 15 / 09 / 2017
*/

int INA = 2;
int ENA = 3;
int INB = 4;
int ENB = 5;
int INC = 6;
int ENC = 7;
int pinABC[3][2]={{2,3},{4,5},{6,7}};//{IN,EN}

int emfABC[3] = {A1,A2,A3};//A B C
int Remf[] = {0,0,0};

                        // IN  ,  EN   //120degree
int signals[12][3] = {  {1,0,0},{1,0,1},
                        {0,1,0},{0,1,1},
                        {0,1,0},{1,1,0},
                        {0,0,1},{1,0,1},
                        {0,0,1},{0,1,1},
                        {1,0,0},{1,1,0}};
                                      //180degree
//int signals[12][3] = {  {1,0,1},{1,1,1}, 
//                        {1,0,0},{1,1,1},
//                        {1,1,0},{1,1,1},
//                        {0,1,0},{1,1,1},
//                        {0,1,1},{1,1,1},
//                        {0,0,1},{1,1,1}};
                       
int i,j,k;
int face;
int sum;
int s_wait = A7;
unsigned long wait = 50;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
unsigned long Time,count;
int delta= 0;
int Lastdelta= -1;
void setup() {
      // put your setup code here, to run once:
      Serial.begin(115200);
      for (i=0;i<3;i++){
          for(j=0;j<2;j++){
            pinMode(pinABC[i][j],OUTPUT);
          }  
          pinMode(emfABC[i],INPUT);  
          
      }
      pinMode(s_wait,INPUT);
      i = 0;
      j = 0;
       delta= 0;
       Lastdelta= -1;
}

void loop() {
     
       for(j = 0;j<3;j++){
          Remf[j] = analogRead(emfABC[j]);
       }
       sum = (Remf[0]+Remf[1]+Remf[2])/3; //get mean of back EMF
       wait = map(analogRead(s_wait),0,1023,1000,1); 
       Serial.println(wait);
       Time = micros();
       
       if(Time - count >= wait){
  
       count = Time;
         
  switch(i){
  //Phase1 A-C
    case 0:
      for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }
      delta = Remf[1]-sum;
      
      break;
      

  //Phase2 B-C
    case 2:
      for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }    
      delta = Remf[0]-sum;
      break;

  //Phase3 B-A
    case 4: 
        for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }
      delta = Remf[2]-sum;
    break;   
  
  //Phase4 C-A
  case 6:
        for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }
      delta = Remf[1]-sum;
      break;

  //Phase5 C-B 
  case 8:
        for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }
      delta = Remf[0]-sum;
      break;

  //Phase6 A-B
  case 10:
          for(j = 0 ; j < 3 ; j++){
              digitalWrite(pinABC[j][0],signals[i][j]);
              digitalWrite(pinABC[j][1],signals[i+1][j]);
         }
      delta = Remf[2]-sum;
  break;
  }
      if (Lastdelta < 0){
            if (delta > 0)
                {
                  Lastdelta=delta; //save the last delta
                  i = i + 2;
                  if (i > 11) {
                    i = 0;
                    }
                }
            }//Zero cross from - to +  

      else if (Lastdelta > 0){
              if (delta < 0)
                  {
                  Lastdelta=delta; //save the last delta
                  i = i + 2;
                  if (i > 11) {
                    i = 0;
                    }
                }
              }//Zero cross from + to - 
  
       }
     
}
